def reverser()
  str = yield
  words = str.split
  words.each do |word|
    word.reverse!
  end
  words.join(" ")
end


def adder(arg = 1)
  num = yield
  num + arg
end


def repeater(arg = 1)
  i = 0
  while i < arg
    yield
    i += 1
  end
end
