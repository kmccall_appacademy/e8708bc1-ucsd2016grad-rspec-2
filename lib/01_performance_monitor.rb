require 'time'

def measure(arg = 1)
  i = 0
  start = Time.now
  while i < arg
    yield
    i += 1
  end
  fin = Time.now
  (fin - start)/arg
end
